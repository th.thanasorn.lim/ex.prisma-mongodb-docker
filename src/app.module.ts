import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './libs/prisma/prisma.module';
import { UsersModule } from './api/users/users.module';
import { ExampleModule } from './api/example/example.module';

@Module({
  imports: [PrismaModule, UsersModule, ExampleModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
