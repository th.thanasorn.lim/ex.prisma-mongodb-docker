import {
  Controller,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Put('/:id')
  async updateUser(@Param('id') id: string, @Res() res: Response) {
    await this.userService.updateUserAddress(id, {
      city: 'Test',
      state: 'Test',
      street: 'Test',
      zip: 1234,
    });
    return res.status(HttpStatus.CREATED).send('Created');
  }

  @Get('/create')
  async createUser(@Res() res: Response) {
    await this.userService.createUser();
    return res.status(HttpStatus.CREATED).send('Created');
  }

  @Post('/test')
  async createTest(@Res() res: Response) {
    await this.userService.updateTest();
    return res.status(HttpStatus.CREATED).send('Created');
  }

  @Get()
  async getUser(@Res() res: Response) {
    const user = await this.userService.getUsers();
    return res.status(HttpStatus.OK).send(user);
  }

  @Get('/list')
  async listUser(@Res() res: Response) {
    const user = await this.userService.getUsers();
    return res.status(HttpStatus.OK).send(user);
    // return res.status(HttpStatus.OK).send(`
    // <ul>
    // ${user.map(
    //   (u) => `<li>
    // <div >
    // ${u.id}
    // </div>
    // <br/>
    // <ul style="padding-left:1rem;">
    // ${
    //   u.address
    //     ? Object.values(u.address).map((address) => `<li>${address}</li>`)
    //     : ''
    // }
    // </ul>
    // </li>`,
    // )}
    // </ul>
    // `);
  }
}
