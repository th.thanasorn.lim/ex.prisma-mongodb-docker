import { Inject, Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/libs/prisma/prisma.service';

@Injectable()
export class UsersService {
  constructor(@Inject(PrismaService) private readonly prisma: PrismaService) {}

  async createUser() {
    return await this.prisma.user.create({
      data: {
        name: 'Rich2',
        email: 'hello3@prisma.com',
        posts: {
          create: [
            {
              title: 'My first post',
              body: 'Lots of really interesting stuff',
              slug: 'my-first-post3',
            },
            {
              title: 'My first post',
              body: 'Lots of really interesting stuff',
              slug: 'my-first-post4',
            },
          ],
        },
      },
    });
  }

  async updateUserAddress(id: string, userAddress: Prisma.AddressCreateInput) {
    return await this.prisma.user.update({
      where: {
        id: id,
      },
      data: {
        address: {
          unset: true,
        },
      },
    });
  }

  // async createTest() {
  //   return await this.prisma.test.create({
  //     data: {
  //       nested: {
  //         name: 'Nested 1',
  //         nested: {
  //           name: 'Nested 2',
  //           nested: {
  //             name: 'Nested 3',
  //           },
  //         },
  //       },
  //     },
  //   });
  // }

  async updateTest() {
    return await this.prisma.test.update({
      where: {
        id: '644b7056fc5a665475aece46',
      },
      data: {
        nestedOne: {
          update: {
            name: 'Hello world',
            nestedTwo: {
              upsert: {
                set: {
                  name: 'Set new',
                },
                update: {
                  name: 'Update new',
                },
              },
            },
          },
        },
      },
      // data: {
      //   nestedOne: {
      //     set: {
      //       name: 'Edit 1',
      //       nestedTwo: {
      //         name: 'Nested Two',
      //         nestedThree: {
      //           name: ['a', 'b', 'c'],
      //         },
      //       },
      //     },
      //   },
      // },
    });
  }

  async getUsers() {
    return await this.prisma.test.findMany({
      where: {
        // OR: [
        //   {
        //     address: null,
        //   },
        //   {
        //     address: {
        //       isSet: false,
        //     },
        //   },
        // ],
      },
    });
  }
}
