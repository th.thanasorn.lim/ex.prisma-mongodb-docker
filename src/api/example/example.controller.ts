import {
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Res,
} from '@nestjs/common';
import { ExampleService } from './example.service';
import { Response } from 'express';

@Controller('example')
export class ExampleController {
  constructor(private readonly exampleService: ExampleService) {}

  @Get()
  async getExamples(@Res() res: Response) {
    const docs = await this.exampleService.list({
      where: {
        nestedOne: {
          is: {
            OR: [
              {
                nestedTwo: {
                  isSet: false,
                },
              },
              {
                nestedTwo: null,
              },
            ],
          },
        },
      },
    });
    return res.status(HttpStatus.OK).send(docs);
  }

  @Get(':id')
  async getOneExample(@Param('id') id: string, @Res() res: Response) {
    const doc = await this.exampleService.findOne(id);
    return res.status(200).json(doc);
  }

  @Post()
  async postExample(@Res() res: Response) {
    const result = await this.exampleService.addTest();
    return res.status(HttpStatus.OK).json(result);
  }

  @Delete(':id')
  async delete(@Param('id') id: string, @Res() res: Response) {
    const result = await this.exampleService.deleteTest(id);
  }
}
