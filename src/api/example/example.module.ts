import { Module } from '@nestjs/common';
import { ExampleService } from './example.service';
import { ExampleController } from './example.controller';
import { PrismaModule } from 'src/libs/prisma/prisma.module';

@Module({
  imports: [PrismaModule],
  providers: [ExampleService],
  controllers: [ExampleController],
})
export class ExampleModule {}
