import { Inject, Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/libs/prisma/prisma.service';

@Injectable()
export class ExampleService {
  constructor(@Inject(PrismaService) private readonly prisma: PrismaService) {}

  async findOne(id: string, args?: Prisma.TestFindFirstArgsBase) {
    return await this.prisma.test.findFirst({
      where: args?.where ? args.where : { id: id },
      ...args,
    });
  }

  async list(args?: Prisma.TestFindManyArgs) {
    return await this.prisma.test.findMany({
      select: {
        id: true,
        nestedOne: true,
      },
    });
  }

  async addTest() {
    return await this.prisma.test.createMany({
      data: [
        {
          nestedOne: {
            name: 'Example 1',
            nestedTwo: {
              name: 'Nested Two',
              nestedThree: {
                name: ['a', 'b', 'c'],
              },
            },
          },
        },
        {
          nestedOne: {
            name: 'Example 2',
            nestedTwo: null,
          },
        },
        {
          nestedOne: {
            name: 'Example 3',
            nestedTwo: {
              name: 'Nested Two',
              nestedThree: null,
            },
          },
        },
      ],
    });
  }

  async deleteTest(id: string) {
    return await this.prisma.test.delete({
      where: {
        id,
      },
    });
  }

  async updateTest(
    id: string,
    deepOne?: string,
    deepTwo?: string,
    deepThree?: string[],
  ) {
    return await this.prisma.test.update({
      where: {
        id: id,
      },
      data: {
        nestedOne: {
          update: {
            name: deepOne,
            nestedTwo: {
              upsert: {
                set: {
                  name: deepTwo,
                },
                update: {
                  name: deepTwo,
                  nestedThree: {
                    upsert: {
                      set: {
                        name: deepThree,
                      },
                      update: {
                        name: deepThree,
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    });
  }
}
