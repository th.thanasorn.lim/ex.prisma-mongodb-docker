import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    const content: string = this.appService.getHello();
    return `<ul>
    <li>
    ${content}
    </li>
    <ul>`;
  }
}
