## Running the app

```bash
# init container
$ docker-compose up -d

# start server in development mode
$ yarn start:dev


```
